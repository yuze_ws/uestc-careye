//
// Created by PulsarV on 18-11-11.
//
#include <iostream>
#include <cstring>

typedef struct {
    unsigned short wheel_1_speed;
    unsigned short wheel_2_speed;
    unsigned short wheel_3_speed;
    unsigned short sum=0;
    bool weel_2_direction;
    bool weel_1_direction;
    bool weel_3_direction;
}WHEEL_DATA;

char *decode(
        bool weel_1_direction,unsigned short wheel_1_speed,
        bool weel_2_direction,unsigned short wheel_2_speed,
        bool weel_3_direction,unsigned short wheel_3_speed
        ){
    WHEEL_DATA wheelData;
    wheelData.weel_1_direction=weel_1_direction;
    wheelData.weel_2_direction=weel_2_direction;
    wheelData.weel_3_direction=weel_3_direction;
    wheelData.wheel_1_speed=wheel_1_speed;
    wheelData.wheel_2_speed=wheel_2_speed;
    wheelData.wheel_3_speed=wheel_3_speed;
    wheelData.sum+=weel_1_direction;
    wheelData.sum+=weel_2_direction;
    wheelData.sum+=weel_3_direction;
    wheelData.sum+=wheel_1_speed;
    wheelData.sum+=wheel_2_speed;
    wheelData.sum+=wheel_3_speed;
    char *buffer=new char[sizeof(WHEEL_DATA)];
    memcpy(buffer,&wheelData,sizeof(WHEEL_DATA));
    return buffer;
}
int main(int argc,char** argv){
    char* buffer=decode(1,200,1,200,1,200);
    WHEEL_DATA newWheelData;
    memcpy(&newWheelData,buffer,sizeof(WHEEL_DATA));
    std::cout<<"WHEEL_DATA大小: " << sizeof(WHEEL_DATA)<<std::endl;
    std::cout<< newWheelData.weel_1_direction<<std::endl;
    std::cout<< newWheelData.wheel_1_speed<<std::endl;
    std::cout<< newWheelData.weel_2_direction<<std::endl;
    std::cout<< newWheelData.wheel_2_speed<<std::endl;
    std::cout<< newWheelData.weel_3_direction<<std::endl;
    std::cout<< newWheelData.wheel_3_speed<<std::endl;
    std::cout<< newWheelData.sum<<std::endl;
    return 0;
}
