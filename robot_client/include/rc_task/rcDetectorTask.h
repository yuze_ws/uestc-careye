//
// Created by Pulsar on 2021/8/26.
//

#ifndef ROBOCAR_RCDETECTORTASK_H
#define ROBOCAR_RCDETECTORTASK_H

#include <rc_task/rcBaseTask.h>
#include <functional>
#include <opencv2/opencv.hpp>

namespace rccore {
    namespace task {
        class DetectorTask : public BaseTask {
        public:
            explicit DetectorTask(std::shared_ptr<common::Context> _pcontext);

            ~DetectorTask();

            void Run() override;

        private:
            void *(*init_callback)();

            void *(*before_detecet_callback)(void *);

            void *(*after_detecet_callback)(void *);

            void (*on_thread_break_callback)(void *);

            void (*detect_callback)(cv::Mat &, void *args, const std::shared_ptr<message::MoveMessage> &);
        };
    }
}

#endif //ROBOCAR_RCDETECTORTASK_H
