//
// Created by PulsarV on 18-10-25.
//

#ifndef ROBOCAR_SLAM_DEVICES_H
#define ROBOCAR_SLAM_DEVICES_H

#include <string>
#include <algorithm>
#include <math.h>
#include <vector>
#include <functional>
#include <rc_mapping/rplidar.h>
#include <rc_system/data_struct.h>
#include <rc_system/context.h>

namespace rccore {
    namespace mapping {
        class SlamDevice {
        public:
            explicit SlamDevice(std::shared_ptr<common::Context> pcontext);

            void
            bind(std::function<void(std::vector<data_struct::DOT>, std::shared_ptr<common::Context>)> call_function);

            void start_motor();

            void start_scan(bool force, _u32 timeout);

            void stop();

            void start(bool force, _u32 timeout);

            void recive();

            void release();

        private:
            std::function<void(std::vector<data_struct::DOT>, std::shared_ptr<common::Context>)> call_function;
            rp::standalone::rplidar::RPlidarDriver *rPlidarDriver = nullptr;
            std::shared_ptr<common::Context> pcontext;
            bool is_start = false;
            bool connectSuccess = false;
        };
    }
}


#endif //ROBOCAR_SLAM_DEVICES_H
