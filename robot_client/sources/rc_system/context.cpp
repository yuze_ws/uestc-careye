//
// Created by Pulsar on 2021/7/13.
//

#include <utility>
#include <rc_system/context.h>
#include <rc_task/rcTaskVariable.h>
#include <rc_log/slog.hpp>

namespace rccore {
    namespace common {
        Context::Context(std::shared_ptr<rccore::common::Config> pconfig) : pconfig(std::move(pconfig)) {

        }

        void Context::Init() {
            slog::info << "IMAGE_MESSAGE_QUEUE_SIZE:" << pconfig->pconfigInfo->IMAGE_MESSAGE_QUEUE_SIZE << slog::endl;
            slog::info << "DSP_SERIAL_MESSAGE_QUEUE_SIZE:" << pconfig->pconfigInfo->DSP_SERIAL_MESSAGE_QUEUE_SIZE
                       << slog::endl;
            slog::info << "IMU_MESSAGE_QUEUE_SIZE:" << pconfig->pconfigInfo->IMU_MESSAGE_QUEUE_SIZE << slog::endl;
            slog::info << "RADAR_MESSAGE_QUEUE_SIZE:" << pconfig->pconfigInfo->RADAR_MESSAGE_QUEUE_SIZE << slog::endl;
            slog::info << "NETWORK_MESSAGE_QUEUE_SIZE:" << pconfig->pconfigInfo->NETWORK_MESSAGE_QUEUE_SIZE
                       << slog::endl;
            slog::info << "MOVE_MESSAGE_QUEUE_SIZE:" << pconfig->pconfigInfo->MOVE_MESSAGE_QUEUE_SIZE << slog::endl;
            pmessage_server.reset(new message::MessageServer(
                    pconfig->pconfigInfo->IMAGE_MESSAGE_QUEUE_SIZE,
                    pconfig->pconfigInfo->DSP_SERIAL_MESSAGE_QUEUE_SIZE,
                    pconfig->pconfigInfo->IMU_MESSAGE_QUEUE_SIZE,
                    pconfig->pconfigInfo->RADAR_MESSAGE_QUEUE_SIZE,
                    pconfig->pconfigInfo->NETWORK_MESSAGE_QUEUE_SIZE,
                    pconfig->pconfigInfo->MOVE_MESSAGE_QUEUE_SIZE
            ));
            task::task_variable::CONTEXT_IS_INITED = true;
        }


        Context::~Context() = default;
    }
}
