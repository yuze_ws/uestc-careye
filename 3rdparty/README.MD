# linux
> Run script _download.sh_ to clone 3rdparty sorce code and build it.  

## Install boost
```bash
sudo apt-get install libboost-date-time-dev libboost-filesystem-dev libboost-iostreams-dev libboost-python-dev libboost-thread-dev libboost-system-dev
``` 
## Install linux build toolchain  
```
sudo apt-get install git build-essential pkg-config libtool automake autopoint gettext
sudo apt-get install libgtkgl2.0-dev libgtk2.0-dev libgtkmm-2.4-dev
sudo apt-get install libcanberra-gtk-module
```  
## 3rdparty Dependies

如果您想要追求极致的性能或者您有Intel的神经网络计算棒的话，这里推荐安装OpenVino版本的OpenCV

* opencv 4.1.2 
  ```bash
  git clone -b 4.1.2 https://github.com/opencv/opencv.git
  ```
* googeltest  
  ```bash
  git clone https://github.com/google/googletest.git
  ```
* mavlink  
  ```bash
  git clone https://github.com/mavlink/mavlink.git
  ```
* openmpi  
  ```bash
  sudo apt install openmpi-bin openmpi-common libopenmpi-dev
  ```
* gflags
  ```bash
  git clone https://github.com/gflags/gflags.git
  ```
* grpc
  ```bash
  git clone https://github.com/grpc/grpc.git
  git submodule update --init
  ```
* flex
  ```bash
  sudo apt-get install flex
  ```
## Download and install Qt5

```url
http://download.qt.io/official_releases/qt/
```


# Windows  
Install higher version than boost 1.67.0

Download and install Qt5
```url
http://download.qt.io/official_releases/qt/
```