message(STATUS "Add MRAA")

find_package(PkgConfig REQUIRED)
set(ENV{PKG_CONFIG_PATH} ${RC_BINARY_DIR}/3rdparty/mraa/install/lib/pkgconfig:ENV{PKG_CONFIG_PATH})

pkg_check_modules(MRAA QUIET mraa)
if (NOT MRAA_FOUND)
    message(STATUS "Generate MRAA")
    execute_process(
            COMMAND cmake -S ${RC_SOURCE_DIR}/3rdparty/mraa -DJSONPLAT=OFF -DCMAKE_INSTALL_PREFIX=${RC_BINARY_DIR}/3rdparty/mraa/install -B ${RC_BINARY_DIR}/3rdparty/mraa
            WORKING_DIRECTORY ${RC_SOURCE_DIR}
    )
    execute_process(
            COMMAND cmake --build ${RC_BINARY_DIR}/3rdparty/mraa
            WORKING_DIRECTORY ${RC_SOURCE_DIR}
    )
    execute_process(
            COMMAND cmake --install ${RC_BINARY_DIR}/3rdparty/mraa
            WORKING_DIRECTORY ${RC_SOURCE_DIR}
    )
    pkg_check_modules(MRAA REQUIRED mraa)
endif ()
include_directories(${MRAA_INCLUDE_DIRS})
message(STATUS "MRAA_INCLUDE_DIRS:${MRAA_INCLUDE_DIRS}")
link_directories(${MRAA_LIBRARY_DIRS})
message(STATUS "MRAA_LIBRARY_DIRS:${MRAA_LIBRARY_DIRS}")
message(STATUS "MRAA_LIBRARIES:${MRAA_LIBRARIES}")
#add_subdirectory(${PROJECT_SOURCE_DIR}/3rdparty/mraa)
#include_directories(${PROJECT_BINARY_DIR}/../3rdparty/mraa/install/include)

