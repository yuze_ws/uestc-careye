#include <radarviewer.h>
#include <QOpenGLWidget>
#include <QPainter>

RadarViewer::RadarViewer(QWidget *parent)
        : QOpenGLWidget(parent) {
}

RadarViewer::~RadarViewer() {
}

void RadarViewer::paintGL() {
    glClear(GL_ACCUM_BUFFER_BIT);

}

void RadarViewer::paintEvent(QPaintEvent *e) {
    QPainter painter;
    painter.begin(this);

    QImage img;
    img.load("./test.jpg");
    painter.drawImage(QPoint(0, 0), img);
    painter.end();
}
