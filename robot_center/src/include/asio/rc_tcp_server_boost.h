//
// Created by pulsarv on 19-5-2.
//

#ifndef UESTC_CAREYE_RC_TCP_SERVER_BOOST_H
#define UESTC_CAREYE_RC_TCP_SERVER_BOOST_H
#include <boost/asio.hpp>
#include <string>
class rc_tcp_server_boost {
public:
    rc_tcp_server_boost(int port,boost::asio::io_service &ios);
    void start_listen();

    ~rc_tcp_server_boost();

private:
    int port;
    std::string address;
    boost::asio::io_service& ios_;

    typedef boost::shared_ptr<boost::asio::ip::tcp::socket> socket_ptr;

    void accept_handler(const boost::system::error_code& _ec, socket_ptr _socket);

    void write_handler(const boost::system::error_code& _ec);
};


#endif //UESTC_CAREYE_RC_TCP_SERVER_BOOST_H
